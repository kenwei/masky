import os
import string
from functools import wraps

import click
import pandas
from flask import Flask, g

from masky.db import DocumentCollection, ensure_db


def create_app(config=None):
    app = Flask(__name__, instance_relative_config=True)

    if config:
        app.config.update(config)
    else:
        app.config.from_pyfile('config.cfg')

    logger = app.logger

    def ensure_doc_store(f):
        """
        decorator to ensure data store prior command execution
        """

        @wraps(f)
        def wrapped(*args, **kwargs):
            store = None if 'store' not in g else g.store
            g.store = ensure_db(store, app.config['DATABASE'])
            return f(*args, **kwargs)

        return wrapped

    def _alter(x):
        """
        alter the given x by following
         - increase its value by 1 if x is numeric
         - capitalize its value if x is a string
        :param x:
        :return:
        """
        return x + 1 if isinstance(x, (int, float)) \
            else (string.capwords(x) if isinstance(x, str) else x)

    def _save_csv_doc(filename, echo=logger.info, transform=None, **kwargs):
        """
        save the given csv document into backend data store
        :param filename: file path of the csv file to be saved
        :param echo: optional, the logging function to be used to trace
                     the save process
        :param transform: optional, the transform function to be used to
                          transform the loaded content
        :param kwargs: options to be used to load the give csv file
        :return: True if the save performed, False otherwise
        """
        echo('... reading file contents')
        df = pandas.read_csv(filename, **kwargs)
        content = transform(df) if transform else df

        echo('... saving loaded content as new revision')
        store = DocumentCollection(os.path.basename(filename), g.store)
        return store.save(content.to_csv(index=False, **kwargs))

    @app.route('/')
    def index():
        return 'Hi, this is Masky!'

    @app.cli.command()
    @click.argument('filename')
    @ensure_doc_store
    def load(filename):
        """
        load the given file into backend data store
        """
        click.echo('Loading %s...' % filename)
        if _save_csv_doc(filename, click.echo, header=None):
            click.echo('... done!')
        else:
            click.echo('... failed!')

    @app.cli.command()
    @click.argument('filename')
    @ensure_doc_store
    def alter(filename):
        """
        alter and load the given file into the backend data store
        """
        click.echo('Altering and loading %s...' % filename)
        if _save_csv_doc(filename,
                         click.echo,
                         lambda df: df.applymap(_alter),
                         header=None):
            click.echo('... done!')
        else:
            click.echo('... failed!')

    @app.cli.command()
    @click.argument('filename')
    @click.option('--limit', default=1,
                  help="limit the return revisions, default as 1")
    @ensure_doc_store
    def fetch(filename, limit):
        """
        fetch the revisions of previously loaded file
        """
        click.echo('Fetching the latest %d loaded revision of %s...\n' %
                   (limit, filename))

        for ts, content in \
                DocumentCollection(filename, g.store).fetch(limit).items():
            click.echo(
                ":: Revision loaded on %s::\n%s\n" % (ts, content)
            )

    @app.cli.command()
    @click.argument('filename')
    @ensure_doc_store
    def drop(filename):
        """
        drop all the loaded revisions of the given file
        """
        click.echo('Dropping all loaded %s revisions ...' % filename)
        if DocumentCollection(filename, g.store).drop():
            click.echo('... done!')
        else:
            click.echo('... failed!')

    return app
