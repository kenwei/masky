import logging
from datetime import datetime

from pymongo import MongoClient, DESCENDING
from pymongo.errors import OperationFailure


def ensure_db(db, url):
    """
    ensure the db instance is connected
    :param db: the db instance to be ensured
    :param url: url to be connected to if the db instance is not
    :return: either the same given db instance if it is already connected
             otherwise the newly connected db instance to the given url
    """
    if not db:
        db = MongoClient(url)
    return db.masky


class DocumentCollection:
    """
    wrapper of mongo collection to simplify CRUD operations
    """

    def __init__(self, name, db):
        """
        :param name: the name of the document collection
        :param db: the connected db instance to associate the collection with
        """
        self.name = name
        self.collection = db[name]

    def save(self, content):
        """
        save the given content into the collection
        :param content: file content
        :return: True if success; False otherwise
        """
        logging.info('about to save new content to %s collection' % self.name)
        try:
            result = self.collection.insert_one({
                'content': content,
                'timestamp': datetime.utcnow()
            })
            return result.acknowledged
        except OperationFailure as e:
            logging.warning('... failed, due to %s' % e)
            return False

    def drop(self):
        """
        drop this collection
        :return: True if success; False otherwise
        """
        logging.info('about to drop the %s collection' % self.name)
        try:
            self.collection.drop()
            return True
        except OperationFailure as e:
            logging.warning('... failed, due to %s' % e)
            return False

    def fetch(self, limit=1):
        """
        fetch the latest content revisions from the collection
        :param limit: optional, the maximum number of revisions to be fetched,
                      default as 1
        :return: dict of the revision:content pair if available
        """
        logging.info('about to fetch the latest saved %s' % self.name)
        found = self.collection.find(
            sort=[('timestamp', DESCENDING)],
            limit=limit,
        )
        return {revision['timestamp']: revision['content']
                for revision in found}
