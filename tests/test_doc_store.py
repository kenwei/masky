from unittest import TestCase

import mongomock

from masky import DocumentCollection


class TestDocumentCollection(TestCase):
    def setUp(self):
        self.db = mongomock.MongoClient().masky

    def tearDown(self):
        self.db.drop_collection('test')

    def test_save_and_fetch(self):
        store = DocumentCollection('test', self.db)
        self.assertTrue(store.save('data'))

        data = list(store.fetch().values())
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0], 'data')

    def test_drop(self):
        store = DocumentCollection('test', self.db)
        self.assertTrue(store.drop())
        self.assertEqual(len(store.fetch()), 0)
