from unittest import TestCase
import mock
import mongomock
import tempfile

from masky import create_app


class TestMasky(TestCase):
    def setUp(self):
        self.app = create_app({'DATABASE': 'somewhere'})

    @mock.patch('masky.ensure_db')
    def test_fetch(self, mock_ensure_db):
        mock_ensure_db.return_value = mongomock.MongoClient().masky

        runner = self.app.test_cli_runner()
        result = runner.invoke(args=['fetch', 'random'])
        self.assertTrue('revision of random' in result.output)

    @mock.patch('masky.ensure_db')
    def test_drop(self, mock_ensure_db):
        mock_ensure_db.return_value = mongomock.MongoClient().masky

        runner = self.app.test_cli_runner()
        result = runner.invoke(args=['drop', 'random'])
        self.assertTrue('done!' in result.output)

    @mock.patch('masky.ensure_db')
    def test_load(self, mock_ensure_db):
        db = mongomock.MongoClient().masky
        mock_ensure_db.return_value = db

        with tempfile.NamedTemporaryFile(mode='wt') as fp:
            fp.write('1,abc,123.456')
            fp.flush()
            runner = self.app.test_cli_runner()
            result = runner.invoke(args=['load', fp.name])
            self.assertTrue('done!' in result.output)

        for collection in db.collection_names():
            for doc in db[collection].find():
                self.assertEqual(doc['content'], '1,abc,123.456\n')

    @mock.patch('masky.ensure_db')
    def test_alter(self, mock_ensure_db):
        db = mongomock.MongoClient().masky
        mock_ensure_db.return_value = db

        with tempfile.NamedTemporaryFile(mode='wt') as fp:
            fp.write('1,abc def,123.456')
            fp.flush()
            runner = self.app.test_cli_runner()
            result = runner.invoke(args=['alter', fp.name])
            self.assertTrue('done!' in result.output)

        for collection in db.collection_names():
            for doc in db[collection].find():
                self.assertEqual(doc['content'], '2,Abc Def,124.456\n')
